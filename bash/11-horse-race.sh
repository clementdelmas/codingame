# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
#
# Copyright
# Original code belongs to @demkada
#

# Initialize
read N

# Loop to store powers
for (( i=0; i<N; i++ )); do
  read Pi
  power[$i]=$Pi
done

# Do sort powers
power=($(for each in ${power[@]}; do echo $each; done | sort -n))

# Initialize loop vars
min_diff=10000000

# Loop on powers
for ((i=0; i<${#power[@]}-1; i++))
do
  # Initialize
  current=${power[$i]}
  next=${power[$i+1]}
  current_diff=$((next-current))

  # Check power
  if [ $current_diff -lt $min_diff ]; then
    # Update minimum difference
    min_diff=$current_diff
  fi
done

# Dislay minimum difference
echo "$min_diff"
