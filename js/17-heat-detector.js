"use strict";

/**
 * Get move of a given minimum and maximum
 *
 * @param (int) min     Minimum
 * @param (int) max     Maximum
 *
 * @return (int) Move step
 */
function getMove(min, max) {
  return Math.ceil((max - min) / 2);
}

/**
 * Get new X value
 *
 * @param (int) x         X
 * @param (string) dir    Direction
 * @param (int) minX      Minimum X value
 * @param (int) maxX      Maximum X value
 * @param (int) width     Real width
 *
 * @return (int) New X value
 */
function getNewX(x, dir, minX, maxX, maxWidth, width) {
  // Initialize
  var add = 0;

  // Check for Right
  if (dir.indexOf('R') !== -1) {
    // Update add
    add = 1 * getMove(x, maxX);
  } else if (dir.indexOf('L') !== -1) {
    // Update add
    add = -1 * getMove(minX, x);
  }

  // Update X
  x += add;

  // Check for max X
  if (0 > x) {
    // Don't move less than 0
    x = 0;
  } else if (x >= width) {
    // Don't move more than width
    x = (width - 1);
  }

  // Return updated x
  return x;
}

/**
 * Get new Y value
 *
 * @param (int) y         Y
 * @param (string) dir    Direction
 * @param (int) minY      Minimum Y value
 * @param (int) maxY      Maximum Y value
 * @param (int) height    Real height
 *
 * @return (int) New Y value
 */
function getNewY(y, dir, minY, maxY, height) {
  // Initialize
  var add = 0;

  // Check for Right
  if (dir.indexOf('U') !== -1) {
    // Update add
    add = -1 * getMove(minY, y);
  } else if (dir.indexOf('D') !== -1) {
    // Update add
    add = 1 * getMove(y, maxY);
  }

  // Update Y
  y += add;

  // Check for max Y
  if (0 > y) {
    // Don't move less than 0
    y = 0;
  } else if (y >= height) {
    // Don't move more than height
    y = (height - 1);
  }

  // Return updated y
  return y;
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

var inputs = readline().split(' ');
var W = parseInt(inputs[0]); // width of the building.
var H = parseInt(inputs[1]); // height of the building.
var N = parseInt(readline()); // maximum number of turns before game over.
var inputs = readline().split(' ');
var X0 = parseInt(inputs[0]);
var Y0 = parseInt(inputs[1]);

var maxX = W;
var maxY = H;
var minX = 0;
var minY = 0;

// Game loop
while (true) {
  // Initialize
  var BOMBDIR = readline(); // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)

  // Check direction to update X
  if (BOMBDIR.indexOf('R') !== -1) {
    minX = X0;
  } else if (BOMBDIR.indexOf('L') !== -1) {
    maxX = X0;
  }

  // Check direction to update Y
  if (BOMBDIR.indexOf('U') !== -1) {
    maxY = Y0;
  } else if (BOMBDIR.indexOf('D') !== -1) {
    minY = Y0;
  }

  // Get updated positions
  X0 = getNewX(X0, BOMBDIR, minX, maxX, W);
  Y0 = getNewY(Y0, BOMBDIR, minY, maxY, H);

  print(X0 + ' ' + Y0); // the location of the next window Batman should jump to.
}
