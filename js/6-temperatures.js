"use strict";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// Initialize
var n = parseInt(readline()); // the number of temperatures to analyse
var temps = readline(); // the n temperatures expressed as integers ranging from -273 to 5526

// Check temperatures length
if (temps.length < 1) {
  print("0");
} else {
  // Initialize
  var temperatures = temps.split(' ');
  var minDiffWithZero = 5526;
  var foundIndex = 0;
  var diffWithZero;

  // Sort temperatures
  temperatures.sort(function (x, y) {
    return x - y;
  });

  // Loop to check best temperature
  for (var i = 0; i < n; i++) {
    // Initialize
    diffWithZero = Math.abs(temperatures[i]);

    // Check temperature
    if (diffWithZero <= minDiffWithZero) {
      // Set new best temperature
      minDiffWithZero = diffWithZero;
      foundIndex = i;
    }
  }

  // Display best temperature
  print(temperatures[foundIndex]);
}
