"use strict";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// Initialize
var LON = parseFloat(readline().replace(',', '.'));
var LAT = parseFloat(readline().replace(',', '.'));
var N = parseInt(readline());

// Initialize loop vars
var minimalDistance = null, bestDefibName = '';
var x, y, d, defibInfos, defibLon, defibLat;

// Loop on defibrillators
// Defibrillators parsed info are:
// 0 : id
// 1 : name
// 2 : Address
// 3 : Phone
// 4 : Lon
// 5 : Lat
for (var i = 0; i < N; i++) {
  // Initialize
  var DEFIB = readline();

  // Get defibrillator info: lon and lat
  defibInfos = DEFIB.split(';');
  defibLon = parseFloat(defibInfos[4].replace(',', '.'));
  defibLat = parseFloat(defibInfos[5].replace(',', '.'));

  // Calculate position
  x = (LON - defibLon) * Math.cos((defibLat + LAT) / 2);
  y = (LAT - defibLat);
  d = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) * 6371;

  // Check minimal distance
  if (minimalDistance === null || d < minimalDistance) {
    // Update minimalDistance
    minimalDistance = d;
    bestDefibName = defibInfos[1];
  }
}

// Display best defib name
print(bestDefibName);
