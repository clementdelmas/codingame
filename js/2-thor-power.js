/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/

var inputs = readline().split(' ');
var lightX = parseInt(inputs[0]); // the X position of the light of power
var lightY = parseInt(inputs[1]); // the Y position of the light of power
var initialTX = parseInt(inputs[2]); // Thor's starting X position
var initialTY = parseInt(inputs[3]); // Thor's starting Y position

// Initialize thor position
var thorX = initialTX;
var thorY = initialTY;
var directionX, directionY;

// game loop
while (true) {
  // Initialize
  readline(); // The remaining amount of turns Thor can move. Do not remove this line.
  directionX = '';
  directionY = '';

  // Check thorX position
  if (thorX > lightX) {
    directionX = 'W';
    thorX += 1;
  } else if (thorX < lightX) {
    directionX = 'E';
    thorX -= 1;
  }

  // Check thorY
  if (thorY > lightY) {
    directionY = 'N';
    thorY -= 1;
  } else if (thorY < lightY) {
    directionY = 'S';
    thorY += 1;
  }

  // A single line providing the move to be made: N NE E SE S SW W or NW
  print(directionY + directionX);
}
