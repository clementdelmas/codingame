/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// game loop
while (true) {
  // Initialize
  var inputs = readline().split(' ');
  var spaceX = parseInt(inputs[0]);
  var spaceY = parseInt(inputs[1]);
  var maxH = 0;
  var destroyY = 0;
  var action = 'HOLD';

  for (var i = 0; i < 8; i++) {
    // represents the height of one mountain, from 9 to 0.
    // Mountain heights are provided from left to right.
    var mountainH = parseInt(readline());

    // Check mountain height
    if (mountainH >= maxH) {
      // Update height
      maxH = mountainH;
      destroyY = i;
    }
  }

  // Check position
  if (spaceX === destroyY) {
    action = 'FIRE';
  }

  // either:  FIRE (ship is firing its phase cannons) or HOLD (ship is not firing).
  print(action);
}
