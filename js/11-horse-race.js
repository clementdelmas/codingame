"use strict";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// Initialize
var N = parseInt(readline());
var powers = [];

// Loop to store powers
for (var i = 0; i < N; i++) {
  powers.push(parseInt(readline()));
}

// Sort powers
powers.sort();

// Initialize loop vars
var currentNumber, nextNumber, currentDiff;
var minDiff = powers[0];

// Loop on powers
for (var i = 0, totalPowers = (powers.length - 1); i < totalPowers; i++) {
  // Initialize
  currentNumber = powers[i];
  nextNumber = powers[i + 1];
  currentDiff = Math.abs((nextNumber - currentNumber));

  // Check diff
  if (currentDiff < minDiff) {
    // Do store current diff
    minDiff = currentDiff;
  }
}

// Display minimum difference between powers
print(minDiff);
