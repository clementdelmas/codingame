"use strict";

/**
 * Get output for a given node
 *
 * @param (Array) nodes             Nodes list
 * @param (int) x                   X index
 * @param (int) y                   Y index
 * @param (string) _errorOutput     Error output
 *
 * @return (string) Output to display
 */
function getOutputFor(nodes, x, y, _errorOutput) {
  // Initialize
  var output = '';
  var nodeExists = true;

  // Check if node exist
  if (nodes[x] !== undefined && nodes[x][y] !== undefined) {
    // Node exist
    if (nodes[x][y] === '0') {
      // Node is valid
      return y + ' ' + x;
    }
  }

  // Node is invalid
  return _errorOutput;
}

/**
 * Get output for right node
 *
 * @param (Array) nodes             Nodes list
 * @param (int) x                   X index
 * @param (int) y                   Y index
 * @param (int) max                 Max Y value
 * @param (string) _errorOutput     Error output
 *
 * @return (string) Output to display
 */
function getOutputForRightNode(nodes, x, y, max, _errorOutput) {
  // Initialize
  var rightNode;
  var tmpY = (y + 1);

  while (tmpY < max) {
    // Get right node
    rightNode = getOutputFor(nodes, x, tmpY, _errorOutput);

    // Check output
    if (rightNode !== _errorOutput) {
      // Found one
      return tmpY + ' ' + x;
    }

    // Update tmpY
    tmpY++;
  }

  // Not found
  return _errorOutput;
}

/**
 * Get output for right node
 *
 * @param (Array) nodes             Nodes list
 * @param (int) x                   X index
 * @param (int) y                   Y index
 * @param (int) max                 Max X value
 * @param (string) _errorOutput     Error output
 *
 * @return (string) Output to display
 */
function getOutputForBottomNode(nodes, x, y, max, _errorOutput) {
  // Initialize
  var bottomNode;
  var tmpX = (x + 1);

  while (tmpX < max) {
    // Get right node
    bottomNode = getOutputFor(nodes, tmpX, y, _errorOutput);

    // Check output
    if (bottomNode !== _errorOutput) {
      // Found one
      return y + ' ' + tmpX;
    }

    // Update tmpX
    tmpX++;
  }

  // Not found
  return _errorOutput;
}


/**
 * Don't let the machines win. You are humanity's last hope...
 **/
var width = parseInt(readline()); // the number of cells on the X axis
var height = parseInt(readline()); // the number of cells on the Y axis
var nodes = [];
var errorOutput = '-1 -1';
var output, currentNodeOutput;

// Loop to store lines
for (var i = 0; i < height; i++) {
  var line = readline(); // width characters, each either 0 or .
  nodes.push(line);
}

// Loop on height
for (i = 0; i < height; i++) {
  // Loop on width
  for (var j = 0; j < width; j++) {
    // Initialize
    currentNodeOutput = getOutputFor(nodes, i, j);

    // Check current node
    if (currentNodeOutput === errorOutput) {
      // Don't add current node
      continue;
    }

    // Add current node
    output = currentNodeOutput + ' ';

    // Get right node output
    output += getOutputForRightNode(nodes, i, j, width, errorOutput) + ' ';

    // Get bottom node output
    output += getOutputForBottomNode(nodes, i, j, height, errorOutput) + ' ';

    // Display node output
    print(output);
  }
}
