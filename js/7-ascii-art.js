/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// Initialize
var L = parseInt(readline());   // Width
var H = parseInt(readline());   // Height
var T = readline();             // Text to display
var textSize = T.length;
T = T.toUpperCase();
var textToDisplay = T.split('');
var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ?'.split('');
var lastIndex = alphabet.length - 1;

// Initialize loop vars
var i, ROW, l, letterToDisplay, letterIndex, rowDisplay;

// Loop on characters to store
for (i = 0; i < H; i++) {
  // Initialize
  ROW = readline();
  rowDisplay = '';

  // Loop on text to display
  for (l = 0; l < textSize; l++) {
    // Initialize
    letterToDisplay = textToDisplay[l].toUpperCase();
    letterIndex = alphabet.indexOf(letterToDisplay);

    // Check index
    if (letterIndex === -1) {
      letterIndex = lastIndex;
    }

    // Update row display
    rowDisplay += ROW.substr((letterIndex * L), L);
  }

  // Display row
  print(rowDisplay);
}
