"use strict";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

/**
 * Converts a given string in binary code
 *
 * @param (string) str    String to convert
 *
 * @return (string) Given string as binary code
 */
function toBin(str) {
  // Initialize
  var st, i, j, d;
  var arr = [];
  var len = str.length;
  for (i = 1; i<=len; i++){
    // Reverse so its like a stack
    d = str.charCodeAt(len-i);
    for (j = 1; j < 8; j++) {
      arr.push(d%2);
      d = Math.floor(d/2);
    }
  }

  // Reverse all bits again.
  return arr.reverse().join("");
}

// Initialize
var MESSAGE = readline();
var messageAsBinary = toBin(MESSAGE);
var messageToDisplay = '';

// Chunk binary message in array
var chunkedMessage = messageAsBinary.match(/(.)\1*/g);
var chunkedMessageLength = chunkedMessage.length;

// Loop on chunked message
for (var i = 0; i < chunkedMessageLength; i++) {
  // Update message to display
  messageToDisplay += (chunkedMessage[i][0] === '1' ? '0' : '00') + ' ' +
                      new Array(chunkedMessage[i].length + 1).join('0') + ' ';
}

// Display message
print(messageToDisplay.trim());
