/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

var road = parseInt(readline()); // the length of the road before the gap.
var gap = parseInt(readline()); // the length of the gap.
var platform = parseInt(readline()); // the length of the landing platform.
var hasJumped = false;
var maxSpeed = platform - (road + gap);

// game loop
while (true) {
  var speed = parseInt(readline()); // the motorbike's speed.
  var coordX = parseInt(readline()); // the position on the road of the motorbike.
  var action = 'WAIT';

  // Check speed
  if (speed < (gap + 1)) {
    // Do speed
    action = 'SPEED';
  } else if (speed > (gap + 1)) {
    action = 'SLOW';
  }

  // Check next action
  if (hasJumped) {
    action = 'SLOW';
  } else {
    if ((coordX + speed) > road) {
      // Don't speed
      action = 'JUMP';
      hasJumped = true;
    }
  }

  print(action); // A single line containing one of 4 keywords: SPEED, SLOW, JUMP, WAIT.
}
