"use strict";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// Initialize
var n = parseInt(readline());
var inputs = readline().split(' ');
var maxDiff = 0;

// Initialize min and max
var _min = parseInt(inputs[0]);
var _max = _min;

// Initialize loop values
var num;

// Loop on numbers
for (var i = 1; i < n; i++) {
  // Initialize
  num = parseInt(inputs[i]);

  // Check for min
  if (num < _min) {
    // Set new min
    _min = num;
  } else if (num > _max) {
    // Check for new max difference
    if (maxDiff < (_max - _min)) {
      // Update max diff
      maxDiff = (_max - _min);
    }

    // Reset min and max
    _min = num;
    _max = num;
  }
}

// Check for last reset
if (maxDiff < (_max - _min)) {
  // Update max diff
  maxDiff = (_max - _min);
}

// Get real value
maxDiff = Math.abs(maxDiff) * -1;

// Display max diff
print(maxDiff);
