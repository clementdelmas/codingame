"use strict";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

var N = parseInt(readline()); // Number of elements which make up the association table.
var Q = parseInt(readline()); // Number Q of file names to be analyzed.
var mimeTypes = {};

// Loop on file extensions and mime types
// Will store these information in an associative array :
// key: file extension in lowercase
// value: MIME type
for (var i = 0; i < N; i++) {
  // Initialize
  var inputs = readline().split(' ');

  // Store mime type in assiative array
  mimeTypes[inputs[0].toLowerCase()] = inputs[1];
}

// Initialize loop vars
var fileName, fileExt, displayedExt;

// Loop on files
for (var i = 0; i < Q; i++) {
  // One file name per line.
  fileName = readline();

  // Initialize
  displayedExt = 'UNKNOWN';

  // Check if file has extension
  if (fileName.indexOf('.') !== -1) {
    // Update displayed extension
    fileExt = fileName.split('.').pop().toLowerCase();
    displayedExt = mimeTypes[fileExt] || displayedExt;
  }

  // Display mime type
  print(displayedExt);
}
